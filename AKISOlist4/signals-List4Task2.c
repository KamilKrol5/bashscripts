#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>

// to kill: kill $(pgrep _file_.o)
void ctrl_c_handler(int dummy) {
    printf("CTRL+C Signal: %d\n",dummy);
}
void signal_handler(int dummy) {
    printf("Signal: %d\n",dummy);
}
int main(int argc, char const *argv[])
{
    if (fork() == 0)
    {
        execlp("bash", "bash", "-c", "man 7 signal | grep 'SIGKILL and'", NULL);
    }
    int status;
    wait(&status);
    int i = 0;
    for (int j = 1; j < SIGRTMAX; j++) {
        //if (j != SIGTSTP)
            signal(j,signal_handler);
    }

    for (int j = 1; j < SIGRTMAX; j++) {
        if (j != SIGKILL && j != SIGSTOP)
        raise(j);
        usleep(100000);
    }
    return 0;
}
