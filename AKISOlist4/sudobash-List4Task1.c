//#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>


int main(int argc, char const *argv[])
{
    setuid(geteuid());
    char *args[] = { "bash", NULL };
    execv("/bin/bash", args);
    //execl("/bin/bash", "bash", NULL);
    return 0;
}
//need to compile, then change owner to root (sudo chown root:root _file_) and set setuid flag (chmod 4701 _file_ OR chmod u+s _file_)
//if chmod u+s _file_ used remove unecessary priviligies such as modifying by any user
