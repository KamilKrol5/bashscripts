#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/wait.h>
#include <limits.h>

const char * const delimiters = " \t\n\r";

void SIGINT_handler()
{
}

void SIGCHLD_handler()
{
    //call waitpid with argument -1 to wait for any child process (kernel knows that)
    //WNOHANG prevents handler from blocking, it tells to return 0 if no child has exited
    //in other words, to return pid of child only when it died, not to wait for child if it is executing 
    while(waitpid(-1,0,WNOHANG) > 0) {} //we could call waitpid only once but we know that signals are not queued, so if more than one child dies
                                        //at once we want to catch them all
}

void print_start()
{
    char working_directory[PATH_MAX];//PATH_MAX is max length a path can has
    printf("\x1b[32mlsh\x1b[0m:");
    getcwd(working_directory,PATH_MAX);
    printf("\x1b[93m%s\x1b[0m$",working_directory);
}

bool is_delimiter(char c) {
    for (const char *tmp = delimiters; *tmp != '\0'; tmp++)
    {
        if (c == *tmp)
        {
            return true;
        }
    }
    return false;
}

int read_from_stdin(char** line) 
{
    //char* line = NULL;
    //*line = NULL;
    size_t size = 0; //size_t is an unsigned integer data type. It's used to represent sizes of objects in bytes 
    ssize_t ret = getline(line, &size, stdin); //There could be some file instead of stdin
    if (ret > 0) //getline returns number of bytes read and we need to check if line is non-empty
        if ((*line)[ret - 1] == '\n') //replace \n with \0
            (*line)[ret - 1] = '\0';
    
    return ret;
}

char** split_input(char* line, int* elements_count_return)
{
    int buffer_size = 128;
    char** elements = calloc(buffer_size, sizeof(char*));
    char* buffer = malloc(sizeof(char) * buffer_size);
    char *current_buff_ptr = buffer;
    char *current_line_ptr = line;

    int elements_counter = 0;
    bool add_to_elements = true;
    do {
        if (is_delimiter(*current_line_ptr))
        {
            if (!add_to_elements)
                *current_buff_ptr++ = '\0';
            add_to_elements = true;
            current_line_ptr++;
            continue;
        }
        else
        {
            *current_buff_ptr = *current_line_ptr;
        }
        if (add_to_elements) 
        {
            add_to_elements = false;
            elements[elements_counter++] = current_buff_ptr;
        }
        current_line_ptr++;
        current_buff_ptr++;

        if (elements_counter >= buffer_size) 
        {
            printf("Full buffer error");
            exit(1);
        }

    } while (*current_line_ptr != '\0');
    *current_buff_ptr = '\0';

    *elements_count_return = elements_counter;
    return elements;
}

int main(int argc, char const *argv[])
{
    //_____________________________________________________SET SIGINT ACTION
    struct sigaction act1; //create sigaction object, sigaction is a struct from signal.h
    sigemptyset(&act1.sa_mask);//initialize sa_mask in act structure to be empty, the mask tells which signals should 
                              //be blocked during executing handler, in this case it's empty
                              //using sigemptyset() because we dont know the type of this mask (sigset_t), it's inner struct on signal.h library
    act1.sa_flags = SA_RESTART;//options which tells how the handler should work
                              //SA_RESTART flag automatically restarts system calls if they were interrupted by a signal
                              //(sending any signal (except ignored ones) interrupts executing system call)
                              //(not all system calls are restarted with this flag)
    act1.sa_handler = SIGINT_handler;
    sigaction(SIGINT, &act1, NULL);//setting sigaction act for signal SIGINT, 3rd arg is place for returnig previous sigaction i.e. act0. (if set)
    //____________________________________________________SET SIGCHLD ACTION
    struct sigaction childaction; 
    sigemptyset(&childaction.sa_mask);
    childaction.sa_flags = SA_RESTART | SA_NOCLDSTOP;
    sigaction(SIGCHLD, &childaction, NULL);
    //____________________________________________________
    while (1) {
        print_start();
        char* out;
        int line_len;
        //______________________________________________HANDLING EXIT
        if ((line_len = read_from_stdin(&out)) == -1 || strcmp(out, "exit") == 0 ) break;
        //______________________________________________determine if program should be executed in the background
        bool background = false;
        if(out[line_len-2] == '&') //if the last char is '&' we want to run the programm in the background
        {
            background = true;
            out[line_len-2] = '\0';//replace '&' with '\0' to avoid giving wrong argument
        }
        int count=0;
        char ** xd = split_input(out,&count);
        if (count != 0)
        {
            //___________________________________________HANDLING CD
            if (strcmp(xd[0],"cd") == 0) 
            {
                if (count < 2 || chdir(xd[1]) == -1) 
                {
                    perror(NULL);
                    printf("Wrong direcory\n");
                }
            }
            else
            {
                pid_t pid;
                
                
                if ((pid = fork()) == 0)
                {//child
                    if (execvp(xd[0],xd) == -1)
                    {
                        printf("Unknown command: %s\n",out);
                        exit(1);
                    }
                }
                else
                {//parent
                    if(!background)//if we run process in the background we do not wait, there is a sigchld hanlder to avoid zombie processes 
                    {   int status;
                        waitpid(pid,&status,0); //wait waits for first child to die, we need to wait for specific child
                    } else {printf("Process %d run in the background\n",pid);}
                }
            }
        }
        free(xd[0]);
        free(xd);
        free(out);
        out = NULL;
        xd[0] = NULL;
    }
    printf("\n");
    return 0;
}
