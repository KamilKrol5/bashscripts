#include <signal.h>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char const *argv[])
{
    for (int j = 1; j < SIGRTMAX; j++) {
        if (kill(1,j) == -1)
        {
            perror("kill");
            //exit(1);
        } else {
            printf("Signal %d sent to init.\n",j);
        }
        usleep(100000);
    }
    return 0;
}

/*
Normalny użytkownik nie może wysłac żadnego sygnału do inita, root moze, ale wtedy ich wysłanie nie przynosi żadnych efektów.
*/