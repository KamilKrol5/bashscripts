#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>

int caught = 0;

void signal_handler(int x) {
    printf("Received signal %d in process %d\n",x,getpid());
    caught++;
}
int main(int argc, char const *argv[])
{
    if(fork()==0) {
        int sent = 0;
        for (int j = 0; j < 21; j++) {
            kill(getppid(),SIGUSR1);
            sent++;
            // usleep(5);
        }
        printf("\nSignals sent: %d\n",sent);

    } else {
        signal(SIGUSR1,signal_handler);
        int status;
        wait(&status);
        printf("\nSignals caught: %d\n",caught);
    }
    
    return 0;
}
/*
Sygnały nie są kolejkowane.
*/