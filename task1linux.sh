#!/bin/bash
trap ctrl_c INT;
ctrl_c() { 
	tput cup $(($max_graph_height+1)) 0; 
	tput cnorm; 
	tput rmcup;
	exit; 
} 
print_bytes()
{
    if [ $1 -lt 1001 ]; then
        echo   "${1}bytes/s";
    elif [ $1 -lt 1000001 ]; then
        printf "%.2fKb/s" "$( echo "scale=0;$1 / 1000" | bc -l )";
    else
        printf "%.2fMb/s" "$( echo "scale=0;$1 / 1000000" | bc -l )";
    fi;
}

#IFS=$'\n'
dev_info="$(cat /proc/net/dev )";
#echo "$dev_info"
time_gap=2;
upper_margin=8;
row_counter=$upper_margin;
real_graph_height=28;
max_graph_height=$(($upper_margin+$real_graph_height));
graph_width=10;
tmp_width=0;
array=();
array_cpu=();
count=0;
avg_bytes_two=0;

#initialize array with zeros
for (( i=0; i < graph_width; i++))
do
    array[$i]=0;
	array_cpu[$i]=0;
done;

update_bytes() {
	bytes=$( cat /proc/net/dev | tail -n +3 | { printf 'a=0\n'; printf 'a+=%s + %s\n' $(awk '{print $2 " " $10}'); printf 'a\n'; } | bc -l );
	bytes_old=${bytes_old:-$bytes} #jezeli bytes_old sa puste to przypisuje bytes jezeli nie to nic nie robi
        avg_bytes=$(( ($bytes - $bytes_old) / 1 ));
	(( avg_bytes_two=($avg_bytes_two*$count+$avg_bytes)/($count+1) ))
        ((count++));
	bytes_old=$bytes;
}
update_array() {
    array=("${array[@]:1}") # stworz tablice array z tego co w nawiasach, to w nawiasach: wez wszstkie argumenty array i pomin 1 pierwszy
    array_cpu=("${array_cpu[@]:1}");
    # for (( i=0; i < $graph_width-1; i++))
    # do
    #     array[$i]=${array[$(($i+1))]:-0};
    #     #echo "++${array[$i]}++"
    # done;
    (( array[$graph_width-1]=$1 ));
    (( array_cpu[$graph_width-1]=$2 ))
}
get_max_from_array() {
    local max=0;
    for val in "${array[@]}";
    do
        (( val > max )) && max=$val;
    done;
    # for (( i=0; i < ${#array[@]}; i++))
    # do
    #     tmp=${array[$i]};
    #     if [ tmp -gt max ]; then max=tmp; fi;
    # done;
    echo $max;
}
print_everything() {
    #RUNTIME
    runtime="$( cat /proc/uptime | awk '{ print $1 }')";
    printf "Runtime:       %d days %d hours %d minutes %2.0f seconds. \n" \
        "$( echo "scale=0;$runtime/60.0/60.0/24 " | bc -l )" \
        "$( echo "scale=0;($runtime/60.0/60.0)%24 " | bc -l )" \
        "$( echo "scale=0;($runtime/60.0)%60 " | bc -l )" \
        "$( echo "scale=0;$runtime%60" | bc -l )";
    #SPEED
    
    		#bytes=$( cat /proc/net/dev | tail -n +3 | awk '
    		#{ bytes += $2 + $10 }
    		#END { printf "%s" , bytes } ' );

    printf "Total speed:   %s        \n" "$( print_bytes $avg_bytes )";
    
    #AVG SPEED
    #echo ${#array[@]}; #length of array
    
    ###echo A2$avg_bytes_two;
    printf "Average speed: %s  \n" "$( print_bytes $avg_bytes_two)";

    #BATTERY
    echo   "Battery:       $( cat /sys/class/power_supply/BAT0/capacity )%                          ";

    #SYSTEM LOAD
    printf "System load:   (over last one minute) %s%%, (over last five minutes) %s%%.           \n" \
        $( cat /proc/loadavg | awk '{print $1*100} {print $2*100}' );

    #PREPARE FOR GRAPH DRAWING
    padding=$(( $graph_width*5+12 ));
    echo "";
    printf "%${padding}s %${padding}s" "SPEED OF DATA TRANSFER"  "AVERAGE SYSTEM OVERLOAD"
    #GRAPH
    tput cup $upper_margin 0; 
    #echo MB$max_bytes "                         " ;
    printf "%10s" "$( print_bytes $max_bytes )";
    tput cup $max_graph_height 0; printf "%10s" "0bytes/s";
    #$tmp_width=0;
    for (( i = 0; i < $graph_width; i++ ))
    do
        limit=$(( $max_graph_height-${array[$i]}/$scale ));
        #row_counter=$max_graph_height;
        column=$((12+$i*5));
        for (( row_counter=$max_graph_height; row_counter >= $upper_margin; row_counter-- ))
        do
            tput cup $row_counter $column;
            if [ $row_counter -gt $limit ]; then
                tput setab 1; 
            fi;
               printf "     ";
                tput sgr0;
        done;
    done;

#GRAPH2

tput cup $upper_margin $padding; 
    printf "%10s" "600%";
    tput cup $max_graph_height $padding; printf "%10s" "0%";
    for (( i = 0; i < $graph_width; i++ ))
    do
        limit=$(( $max_graph_height-${array_cpu[$i]}/$scale_cpu ));
        column=$((12+$i*5+$padding));
        for (( row_counter=$max_graph_height; row_counter >= $upper_margin; row_counter-- ))
        do
            tput cup $row_counter $column;
            if [ $row_counter -gt $limit ]; then
                tput setab 4; 
            fi;
               printf "     ";
                tput sgr0;
        done;
    done;



}
# tput civis;
#tput init;
tput smcup;
tput civis;    
avg_bytes=0;
while true;
do
    sleep $time_gap &
    max_bytes=$(get_max_from_array);
    scale=$( echo "scale=0;$max_bytes/$real_graph_height" | bc -l );
	scale_cpu=$(( 600/$real_graph_height ))
    (( scale = scale > 0 ? scale : 1 ))
    #tput rc;
    sleep_pid=$!
    update_array "$( echo "scale=0;$avg_bytes/1" | bc -l )" "$( cat /proc/loadavg | awk '{print $1*100}' | bc -l )";
    tput home;
    update_bytes;
    printf '%s'  "$( print_everything; )";
    wait $sleep_pid
    #print_everything;
done;
