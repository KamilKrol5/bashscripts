#!/bin/bash
echo "  PID       PPID    OPENED FILES          NAME    STATE  PRIORITY    NICE";
for file in /proc/*
do
    #stat_caption="$( echo $file | echo "$(cat -)"/stat | grep [0-9] )";
    stat_caption="$( echo "$file"/stat | grep '[0-9]' )";

    #if [[ $file =~ ^.*/[0-9]+$ ]] ## sprawdza czy $file pasuje do regexa
    if [ $stat_caption ]; then 
        stat="$( cat $stat_caption )";

        # pid="$( awk -F' ' '{ print $1 }' <( echo $stat) )";
        # ppid="$( awk -F' ' '{ print $4 }' <( echo $stat) )";
        # name="$( awk -F' ' '{ print $2 }' <( echo $stat) )";
        # state="$( awk -F' ' '{ print $3 }' <( echo $stat) )";
        # priority="$( awk -F' ' '{ print $18 }' <( echo $stat) )";
        # nice="$( awk -F' ' '{ print $19 }' <( echo $stat) )";
        fd_count="$(ls  /proc/$pid/fd/ 2>/dev/null | wc -l )" ;

        read -r pid name state ppid priority nice < <(echo $stat | awk '{print $1 " " $2 " " $3 " " $4 " " $18 " " $19 }')

        printf "%5s      %5s  %9s    %15s     %1s     %2s        %2s\n" $pid $ppid $fd_count $name $state $priority $nice;
    fi;
done;