#!/bin/bash
original_page_file=/tmp/originalFile;
lynx -dump $1 > $original_page_file;
while true;
do
    sleep $2;
    url=$1;
    filename="$( echo ${url}$(date) | sed -r 's#[.:/ ]##g' )"; #substitute - s
    #filename="${url//"/"/}${date//" "/}";

    if [ "$(diff -q $original_page_file <( lynx -dump $1 | tee /tmp/p ) )" ]; then    #<( lynx -dump $1 )
        #echo $(diff -q page "$(lynx -s -dump $1)");
        if ! [ -d pageHistory ]; then mkdir pageHistory; fi;
        cp /tmp/p pageHistory/$filename;
        echo "Page changed: $1";
    fi;
    mv /tmp/p $original_page_file;
done;