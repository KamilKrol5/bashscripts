#define _GNU_SOURCE
#include <stdio.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <pthread.h>

//compile with:  gcc -pthread

int row = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int number_of_threads = 0;
int number_of_rows = 0;
char** matrix1;
char** matrix2;
char** matrixR;

void multiply(char** M1,char** M2, char** MRes,int row_number)
{
    char sum = 0;//zakladamy ze pierwsze wspolrzedna to wiersz
    for (int f = 0;f < number_of_rows; f++)//po kolumnach mnoznika i kolumnach wyniku
    {
        sum = 0;
        for (int b = 0; b < number_of_rows; b++)//po elementach wierszy mnoznej (kolumnach) i po wierszach mnoznika 
        {
            if (matrix2[row_number][b] != 0 && matrix1[b][f] != 0)
            {
                sum = 1;
                break;
            }
        }
        matrixR[row_number][f] = sum;
    }
}

void* multiply_matrices(void* arg)
{
    int *counter = (int*)arg;
    *counter = 0;
    while(1) 
    {
        if (pthread_mutex_lock(&mutex) > 0)
        {
            exit(1); 
        }
        int r = row;
        row++;
        if (pthread_mutex_unlock(&mutex) > 0)
        {
            exit(1);
        }
        if (r >= number_of_rows)
        {
            break;
        }
        multiply(matrix1,matrix2,matrixR,r);
        *counter += 1;
    }
}

void allocate_mem_for_matrixes(int size)
{
    matrix1 = malloc(size * sizeof(*matrix1));
    matrix2 = malloc(size * sizeof(*matrix2));
    matrixR = malloc(size * sizeof(*matrixR));
    for (int i = 0; i < size; i++)
    {
        matrix1[i] = malloc(size * sizeof(**matrix1));
        matrix2[i] = malloc(size * sizeof(**matrix2));
        matrixR[i] = malloc(size * sizeof(**matrixR));
    }
}

void fill_matrices_with_random_data()
{
    time_t t;
    srand(time(&t));
    for (int i = 0; i < number_of_rows; i++)
    {
        for (int u = 0; u < number_of_rows; u++)
        {
            matrix1[i][u] = rand()%2;
            matrix2[i][u] = rand()%2;
        }
    }
}

int main(int argc, char const *argv[])
{
    if (argc < 3)
    {
        printf("Too few arguments.");
        return -1;
    }
    number_of_rows = atoi(argv[1]);
    number_of_threads = atoi(argv[2]);
    if (number_of_rows < 1 || number_of_threads < 1) 
    {
        printf("Wrong argument.");
    }
    allocate_mem_for_matrixes(number_of_rows);
    fill_matrices_with_random_data();
    pthread_t threads[number_of_threads];
    int counters[number_of_threads];
    for (int k=0;k<number_of_threads;k++)
    {
        counters[k]=0;
        int t;
        if ((t = pthread_create(&threads[k],NULL,multiply_matrices,(void*)&counters[k])) != 0) 
        {
            errno = t;
            perror("pthread_create");
            exit(1);
        }
    }
    for (int u = 0;u < number_of_threads;u++)
    {
        pthread_join(threads[u],NULL);
    }
    for (int j = 0; j < number_of_threads; j++)
    {
        printf("Thread %d computed %3d rows\n",j+1,counters[j]);
    }
    return 0;
}
