#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>

int main(int argc, char const *argv[])
{
    int n = 100;
    if (argc > 1)
    {
        char *last;
        n = strtol(argv[1], &last, 10);
        if (argv[1][0] == '\0' || *last != '\0')
        {
            //printf("Given input is not a valid number.\n");
            printf("Usage: %s <matrix size>\n", argv[0]);
            exit(1);
        }
    }
    //--------------------------------------allocating memory for 3 matrices
    int **matrix1 = malloc(n * sizeof(*matrix1));
    int **matrix2 = malloc(n * sizeof(*matrix2));
    int **matrixR = malloc(n * sizeof(*matrixR));
    for (int i = 0; i < n; i++)
    {
        matrix1[i] = malloc(n * sizeof(int));
        matrix2[i] = malloc(n * sizeof(int));
        matrixR[i] = malloc(n * sizeof(int));
    }
    //--------------------------------------assigning random values
    int rng = open("/dev/urandom", O_RDONLY);
    
    for (int i = 0; i < n; i++)
    {
        read(rng, matrix1[i], n * sizeof(int));
        read(rng, matrix2[i], n * sizeof(int));
        //read(rng, matrixR[i], n * sizeof(int));
        // for (int u = 0; u < n; u++)
        // {
        //     matrix[h][u] = rand();
        // }
    }
    struct timeval time1; 
    gettimeofday(&time1,NULL);
    //multiplying M2 x M1
    long long sum1 = 0;//zakladamy ze pierwsze wspolrzedna to wiersz
    for (int p = 0; p < n; p++)//po wierszach mnoznej i wierszach wyniku
    {
        for (int f = 0;f < n; f++)//po kolumnach mnoznika i kolumnach wyniku
        {
            for (int b = 0; b < n; b++)//po elementach wierszy mnoznej (kolumnach) i po wierszach mnoznika 
            {
                sum1 = sum1 + matrix2[p][b] * matrix1[b][f];
            }
            matrixR[p][f] = sum1;
            sum1 = 0;
        }
    }
    struct timeval time2; 
    gettimeofday(&time2,NULL);
    printf("Normal multiplication: %lld\n",(long long)(time2.tv_sec-time1.tv_sec));

    int tmp;
    for (int t = 0; t < n; t++)
    {
        for (int i = 0; i < n; i++)
        {
            tmp = matrix1[t][i];
            matrix1[t][i] = matrix1[i][t];
            matrix1[i][t] = tmp;
        }
    }
    struct timeval time3;
    gettimeofday(&time3,NULL);
    //multiplying M2 x M1
    long long sum2 = 0;//zakladamy ze pierwsze wspolrzedna to wiersz
    for (int p = 0; p < n; p++)//po wierszach mnoznej i wierszach wyniku
    {
        for (int f = 0;f < n; f++)//po kolumnach mnoznika i kolumnach wyniku
        {
            for (int b = 0; b < n; b++)//po elementach wierszy mnoznej (kolumnach) i po wierszach mnoznika 
            {
                sum2 = sum2 + matrix2[p][b] * matrix1[f][b];
            }
            matrixR[p][f] = sum2;
            sum2 = 0;
        }
    }
    struct timeval time4; 
    gettimeofday(&time4,NULL);
    printf("Cache optimalized multiplication: %lld\n",(long long)(time4.tv_sec-time3.tv_sec));
    //printf("%ld",(long)time3.tv_sec);
    return 0;
}
