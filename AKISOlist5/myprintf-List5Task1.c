#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <memory.h>

//man 3 stdarg
//compile with:
//gcc -fno-stack-protector -m32 myprintf-List5Task1.c

void convert_to_string_and_print(int number, int base)
{
    char digits[] = {"0123456789ABCDEF"};
    int negative = 0;
    if (number < 0)
    {
        negative = 1;
        number *= (-1);
    }
    char *inverted_val = (char *)malloc(100 * sizeof(char));
    char *val = (char *)malloc(100 * sizeof(char));
    int counter = 0;
    while (number)
    {
        inverted_val[counter++] = digits[number % base];
        number /= base;
    }
    int j = 0;
    if (negative)
    {
        j = 1;
        val[0] = '-';
    }
    for (int i = counter - 1; i >= 0; i--)
    {
        val[j++] = inverted_val[i];
    }
    for (int i = 0; val[i] != '\0'; i++)
    {
        write(1, &val[i], sizeof(char));
    }
    
}

void myprintf(const char *fmt, ...)
{
    char *p = (char *)&fmt + sizeof(fmt);
    char prev = ' ';
    for (const char *c = fmt; *c != '\0'; c++)
    {
        if (prev == '%')
        {
            switch (*c)
            {
            case 'd':
            {
                convert_to_string_and_print(*(int *)p, 10);
                p += sizeof(int);
                break;
            }
            case 'x':
            {
                convert_to_string_and_print(*(int *)p, 16);
                p += sizeof(int);
                break;
            }
            case 'b':
            {
                convert_to_string_and_print(*(int *)p, 2);
                p += sizeof(int);
                break;
            }
            case 's':
            {
                char *output = *(char **)p;
                for (int i = 0; output[i] != '\0'; i++)
                {
                    write(1, &output[i], sizeof(char));
                }
                p += sizeof(char *);
                break;
            }
            default:
                break;
            }
        }
        else if (prev != '%' && *c != '%')
        {
            write(1, (const char *)c, sizeof(char));
        }
        prev = *c;
    }
}

int main(int argc, char const *argv[])
{
    myprintf("%d %x %b %s\n", 12, 16, 1, "XDDD");
    return 0;
}
