#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <unistd.h>

//compile with:
//gcc -fno-stack-protector -m32 myscanf-List5Task1.c

int min(int x,int y)
{
    if (x > y) return y;
    return x; 
}

void convert_char_array_to_number(char* char_array, int* destination, int base)
{
    printf("converting \"%s\"\n", char_array);
    *destination = 0;
    for (int i = 0; i < (int)strlen(char_array); i++)
    {
        if (char_array[i] >= '0' && char_array[i] < '0' + min(base, 10))
            *destination += (char_array[i] - '0');
        else if (base > 10 && char_array[i] >= 'A' && char_array[i] < 'A' + base)
            *destination += (char_array[i] - 'A' + 10);
        else if (base > 10 && char_array[i] >= 'a' && char_array[i] < 'a' + base)
            *destination += (char_array[i] - 'a' +10);
        else {
            *destination /= base;
            return;
        }
        if (i < strlen(char_array) - 1)
            *destination *= base;
    }
}

void myscanf(const char *fmt, ...)
{
    fflush(stdout);
    char *p = (char *)&fmt + sizeof fmt;
    char prev = ' ';
    for (const char *c = fmt; *c != '\0'; c++)
    {
        if (prev == '%')
        {
            switch (*c)
            {
            case 'd':
            {
                int* ptr = *(int **)p;
                char chars[100];
                char curr_char;
                int counter = 0;
                while(1)
                {
                    read(0, &curr_char, 1);
                    if (curr_char == '\n')
                    {
                        chars[counter] = '\0';
                        break;
                    }
                    chars[counter++] = curr_char;
                }
                convert_char_array_to_number(chars,ptr,10);
                p += sizeof(int *);
                break;
            }
            case 'x':
            {
                int* ptr = *(int **)p;
                char chars[100];
                char curr_char;
                int counter = 0;
                while(1)
                {
                    read(0, &curr_char, 1);
                    if (curr_char == '\n')
                    {
                        chars[counter] = '\0';
                        break;
                    }
                    chars[counter++] = curr_char;
                }
                convert_char_array_to_number(chars,ptr,16);
                p += sizeof(int *);
                break;
            }
            case 'b':
            {
                int* ptr = *(int **)p;
                char chars[100];
                char curr_char;
                int counter = 0;
                while(1)
                {
                    read(0, &curr_char, 1);
                    if (curr_char == '\n')
                    {
                        chars[counter] = '\0';
                        break;
                    }
                    chars[counter++] = curr_char;
                }
                convert_char_array_to_number(chars,ptr,2);
                p += sizeof(int *);
                break;
            }
            case 's':
            {
                char *ptr = *(char **)p;
                char chars[100];
                char curr_char = '\0';
                int counter = 0;
                while (curr_char != '\n')
                {
                    read(0, &curr_char, 1);
                    if (curr_char == '\n')
                        break;
                    chars[counter++] = curr_char;
                }
                chars[counter] = '\0';
                strcpy(ptr, chars);
                p += sizeof(char *);
                break;
            }
            default:
                break;
            }
        }
        else
        {
            
        }
        prev = *c;
    }
}

int main()
{
    int dec;
    printf("Input in DEC\n");
    myscanf("%d", &dec);
    printf("Value: %d\n", dec);
    int bin;
    printf("Input in BIN\n");
    myscanf("%b", &bin);
    printf("Value: %d\n", bin);
    int hex;
    printf("Input in HEX\n");
    myscanf("%x", &hex);
    printf("Value: %d\n", hex);
    char *str = malloc(100);
    printf("Input in char*\n");
    myscanf("%s", str);
    printf("Value: %s\n", str);
}