#ifndef MINITALK_FORK_SOCKETLIB_H
#define MINITALK_FORK_SOCKETLIB_H
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>

//Socketlib przepisany z wykladu

int server_tcp_socket(char *hn, int port)
{
    struct sockaddr_in sn;
    int s;
    struct hostent *he;

    if (!(he = gethostbyname(hn)))
    {
        fprintf(stderr, "gethostbyname()");
        exit(1);
    }
    bzero((char *)&sn, sizeof(sn)); //zeruje
    sn.sin_family = AF_INET;
    sn.sin_port = htons((unsigned short)port);
    sn.sin_addr = *(struct in_addr *)(he->h_addr_list[0]);

    if ((s = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket()");
        exit(1);
    }

    if (bind(s, (struct sockaddr *)&sn, sizeof(sn)) == -1)
    {
        perror("bind()");
        exit(1);
    }

    return s;
}

int accept_tcp_connection(int s)
{
    struct sockaddr_in sn;
    socklen_t l = sizeof(sn);
    int x;

    sn.sin_family = AF_INET;

    if (listen(s, 1) == -1)
    {
        perror("listen()");
        exit(1);
    }

    x = accept(s, (struct sockaddr *)&sn, &l);
    if (x == -1)
    {
        perror("accept()");
        exit(1);
    }
    return x;
}

#endif //MINITALK_FORK_SOCKETLIB_H