#include <stdio.h>
#include <memory.h>
#include <stdlib.h>
#include "socketlib.h"

typedef struct
{
    int fd;
    int id;
} client;

int main(int argc, char const *argv[])
{
    if (argc < 1)
    {
        exit(1);
    }
    printf("Server run\n");
    int client_count = 0;
    client *client_list;
    int s = server_tcp_socket("localhost", atoi(argv[1]));
    fd_set copy;
    fd_set fd_set_main;
    listen(s, SOMAXCONN); //makro telling max amount of connections
    client_list = malloc(SOMAXCONN * sizeof(client));
    FD_ZERO(&fd_set_main);   //zerujemy
    FD_SET(s, &fd_set_main); //dodajemy do mastera file desriptor s
    while (1)
    {
        memcpy(&copy, &fd_set_main, sizeof(fd_set)); //kopiujemy mastera do copy bo select go zniszczy
        select(FD_SETSIZE, &copy, NULL, NULL, NULL); //select na kopii, zwraca do copy odfiltrowane file descriptors
        for (int fd = 0; fd < FD_SETSIZE; fd++)      //idziemy po wszystkich fd w copy
        {
            if (FD_ISSET(fd, &copy))
            { //jesli fd sie zmienil
                //2 przypadki
                if (fd == s) //nowe polaczenie
                {
                    int new = accept_tcp_connection(s);
                    char mess[100];
                    int ID = rand() % 1000;
                    snprintf(mess, 100, "Connection established!\nYour Login: %d\nPeople on chat:\n", ID);
                    write(new, mess, strlen(mess));
                    char xd[30];
                    char xd2[20];
                    snprintf(xd, 30, "%d joined!\n", ID);
                    for (int i = 0; i < client_count; i++)
                    {
                        snprintf(xd2, 20, "%d\n", client_list[i].id);
                        write(new, xd2, strlen(xd2));
                        write(client_list[i].fd, xd, strlen(xd));
                    }
                    client_list[client_count].id = ID;
                    client_list[client_count++].fd = new;
                    FD_SET(new, &fd_set_main);
                }
                else //nowa wiadomosc
                {
                    char input[512];
                    int read_res = read(fd, &input, 512); //czytamy wiadomosc
                    input[read_res] = '\0';
                    if (read_res <= 0) //pusta wiadomosc
                    {
                        close(fd); //usuwamy goscia
                        FD_CLR(fd, &fd_set_main);
                    }
                    else //mamy wiadomosc
                    {
                        char xdd[30];
                        int des_fd;
                        int sender_id;
                        char *message = strtok(input, " ");
                        for (int i = 0; i < client_count; i++)
                        {
                            if (atoi(message) == client_list[i].id) //sprawdzamy pierwszy argument, jest to id goscia do ktorego chcemy cos wyslac
                            {
                                des_fd = client_list[i].fd;
                                //break;
                            }
                            if (fd == client_list[i].fd) //szukamy przy okazji od kogo wiadomosc przyszla
                            {
                                sender_id = client_list[i].id;
                            }
                        }
                        snprintf(xdd, 30, "Message from %d: ", sender_id);
                        write(des_fd, xdd, strlen(xdd));
                        message = strtok(NULL, "");              //bierzemy reszte wiadomosci
                        write(des_fd, message, strlen(message)); //i tu ja wysylamy
                    }
                }
            }
        }
    }
}
