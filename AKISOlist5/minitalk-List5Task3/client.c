#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    char *arr[4];
    arr[0] = "telnet";
    arr[1] = "localhost";
    arr[2] = "8888";
    arr[3] = NULL;
    if (argc > 1)
    {
        arr[2] = argv[1];
    }
    if (execvp(arr[0], arr) == -1)
    {
        printf("error occured\n");
    };
    return 0;
}
