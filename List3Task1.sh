#!/bin/bash
print_bytes()
{
    if [ $1 -lt 1001 ]; then
        echo   "${1}bytes/s";
    elif [ $1 -lt 1000001 ]; then
        printf "%.2f" "$( echo '$1 / 1000' | bc -l )Kb/s";
    else
        printf "%.2f" "$( echo '$1 / 1000000' | bc -l )Mb/s";
    fi;
}

#IFS=$'\n' wont work because cat is in command substitution
dev_info="$(cat /proc/net/dev )";
#echo "$dev_info"
time_gap=2;
upper_margin=6;
row_counter=$upper_margin;
max_graph_height=$(($upper_margin+10));
graph_width=5;
tmp_width=0;
count=0;
avg_bytes_two=0;
array=();
# tput civis;
tput init;
#tput civis;
while true;
do
    tput rc;
    runtime="$( cat /proc/uptime | awk -F' ' '{ print $1 }')";
    printf "Runtime:     %d days %d hours %d minutes %2.0f seconds.                  \n" \
        "$( echo "scale=0;$runtime/60.0/60.0/24 " | bc -l )" \
        "$( echo "scale=0;($runtime/60.0/60.0)%24 " | bc -l )" \
        "$( echo "scale=0;($runtime/60.0)%60 " | bc -l )" \
        "$( echo "scale=0;$runtime%60" | bc -l )";

    bytes=$( cat /proc/net/dev | tail -n +3 | awk '
    { bytes += $2 + $10 }
    END { print bytes } ' );
    bytes_old=${bytes_old:-$bytes} #jezeli bytes_old sa puste to przypisuje bytes jezeli nie to nic nie robi
    avg_bytes=$(( ($bytes - $bytes_old) / $time_gap ));
    printf "Total speed: %s               \n" "$( print_bytes $avg_bytes )";
    
    for (( i=0; i < $graph_width-1; i++))
    do
        array[$i]=${array[$(($i+1))]:-0};
        #echo "++${array[$i]}++"
    done;
    ((array[$graph_width-1]=$RANDOM%11));
    
    #echo ${#array[@]}; #length of array
    (( avg_bytes_two=($avg_bytes_two*$count+$avg_bytes)/($count+1) ))
    (($count+1));
    printf "Average speed: %s  \n" "$( print_bytes $avg_bytes_two)";

    echo   "Battery:     $( cat /sys/class/power_supply/battery/capacity )%                          ";

    printf "System load: (over last one minute) %s%%, (over last five minutes) %s%%.           \n" \
        $( cat /proc/loadavg | awk -F' ' '{print $1*100} {print $2*100}' );
        
    tput cup $upper_margin 0; 
    printf "10 kg";
    tput cup $max_graph_height 0; printf "0 kg";
    #$tmp_width=0;
    for (( i=0; i < $graph_width; i++))
    do
        limit=$(( $max_graph_height-${array[$i]} ));
        #row_counter=$max_graph_height;
        column=$((10+$i*5));
        for (( row_counter=$max_graph_height; row_counter >= $upper_margin; row_counter-- ))
        do
            tput cup $row_counter $column;
            if [ $row_counter -gt $limit ]; then
                tput setab 5; 
            fi;
                printf "   ";
                tput sgr0;
        done;
    #     row_counter=$max_graph_height;
    #     while [ $row_counter -lt $(( $max_graph_height + 1)) ];
    #     do
    #         #echo $row_counter $(($i*5))
    #         tput cup $row_counter $(($i*5)) ;
    #         if [ $row_counter -lt $(($upper_margin+${array[$i]}+1)) ];then
    #         echo XDD;
    #         else 
    #         echo "   ";
    #         fi;
    #         row_counter=$(($row_counter+1));
    #     done;
    #     sleep $time_gap;
    #     bytes_old=$bytes;
    done;
   
done;