
                opt f-g-h+l+o+
                org $1000

start           equ *

                lda <text
                sta $80
                lda >text
                sta $81
                ldy #0
                lda #$a5
                jsr hex

                lda <text
                ldx >text
                jsr $ff80
                brk

hex             sta part1
                :4 lsr part1
                and #$F
                sta part2
                lda part1
                clc
                cmp #10
                bcc less_than_10 ; < 10
                ; >= 10
                sub #10
                add #'A'
                sta $2000
                jmp second_number
less_than_10    add #'0'
                sta $2000
second_number   lda part2
                clc
                cmp #10
                bcc less_2than_10 ; < 10
                ; >= 10
                add #'A'
                sub #10
                sta $2001
                rts
less_2than_10   add #'0'
                sta $2001
                rts

part1           dta b(0)

part2           dta b(0)

                org $2000
text            equ *
                dta b(0),b(0)
                dta b(10) ; '\n'
                dta b(0)

                org $2E0
                dta a(start)

                end of file