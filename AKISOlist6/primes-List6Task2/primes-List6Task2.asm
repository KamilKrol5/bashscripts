section .text
        global main
        extern printf
is_prime:
        push    ebp
        mov     ebp, esp
        sub     esp, 16
        cmp     DWORD [ebp+8], 0
        je      .L2
        cmp     DWORD [ebp+8], 1
        jne     .L3
.L2:
        mov     eax, 0
        jmp     .L4
.L3:
        mov     DWORD [ebp-4], 2
.L7:
        mov     eax, DWORD [ebp+8]
        mov     edx, eax
        shr     edx, 31
        add     eax, edx
        sar     eax, 1
        cmp     DWORD [ebp-4], eax
        jg      .L5
        mov     eax, DWORD [ebp+8]
        cdq
        idiv    DWORD [ebp-4]
        mov     eax, edx
        test    eax, eax
        jne     .L6
        mov     eax, 0
        jmp     .L4
.L6:
        add     DWORD [ebp-4], 1
        jmp     .L7
.L5:
        mov     eax, 1
.L4:
        leave
        ret    
main:
        lea     ecx, [esp+4]
        and     esp, -16
        push    DWORD [ecx-4]
        push    ebp
        mov     ebp, esp
        push    ecx
        sub     esp, 20
        mov     DWORD [ebp-12], 0
.L11:
        cmp     DWORD [ebp-12], 10000
        jg      .L9
        push    DWORD [ebp-12]
        call    is_prime
        add     esp, 4
        test    al, al
        je      .L10
        sub     esp, 8
        push    DWORD [ebp-12]
        push    f
        call    printf
        add     esp, 16
.L10:
        add     DWORD [ebp-12], 1
        jmp     .L11
.L9:
        mov     eax, 0
        mov     ecx, DWORD [ebp-4]
        leave
        lea     esp, [ecx-4]
        ret

section .data
        f: db "%d ",10,0