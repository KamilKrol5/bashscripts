#include <stdio.h>
#include <stdbool.h>

/*
to see assembler code (file compiled with -m32 flag):
objdump -d -M intel primes-List6Task2.o
*/
bool is_prime(int n)
{
    if (n == 0 || n == 1)
        return false;
    for (int i = 2; i <= n / 2; i++)
    {
        if (n % i == 0)
        {
            return false;
        }
    }
    return true;
}

int main()
{
    for (int i = 0; i < 10000; i++)
    {
        if (is_prime(i))
            printf("%d ", i);
    }

    return 0;
}
//gcc -m32 -S -masm=intel -o primes-List6Task2.s primes-List6Task2.c