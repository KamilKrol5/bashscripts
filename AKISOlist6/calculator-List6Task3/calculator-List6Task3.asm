section .data
    input1:         DQ 0.0
    input2:         DQ 0.0
    operator:       DB 0
    welcome:        DB "Type arithmetic operation:",10,0 ;10 is \n and 0 is \0
    scanf_format:   DB "%lf %c %lf",0
    output_format:  DB "%lf",10,0

section .text
    global main
    extern printf
    extern scanf

main:
    push welcome ;prepare fo calling printf, pushes pointer to first sign of welcome, pointer has 4 byte (32bits) 
    call printf
    add esp,4 ;clean after printf, 4 because welcome is a pointer
    ;prepare to call scanf
    push input2
    push operator
    push input1
    push scanf_format
    call scanf
    add esp,16 ;clean after scanf,8,4 (pointer),8,4 (pointer)
    finit
    fld QWORD [input1]
    fld QWORD [input2]
    cmp byte [operator],'+'
    je addition
    cmp byte [operator],'-'
    je subtraction
    cmp byte [operator],'*'
    je multiplication
    cmp byte [operator],'/'
    je division

addition:
    fadd st1,st0
    jmp print
subtraction:
    fsub st1,st0
    jmp print
multiplication:
    fmul st1,st0
    jmp print
division:
    fdiv st1,st0
    jmp print
    

print:
    fstp QWORD [input1]
    fst QWORD [input1]
    push DWORD[input1+4]
    push DWORD[input1]
    push output_format
    call printf
    add esp,12

mov ebx, 1
mov eax, 0
int 0x80
