section .data
    argument: DQ 0.0
    sinh: DQ 0.0
    arcsinh: DQ 0.0
    scanf_format: DB "%lf",0
    output_format: DB "Sinh(x)=%lf",10,"Arcsinh(x)=%lf",10,0
    instruction: DB "Program computes sinh and sinh-1. Type argument:",10,0
    SaveCW: DW 0
    MaskedCW: DW 0
section .text
    global main
    extern printf
    extern scanf

main:
    push instruction
    call printf
    add esp,4

    push argument
    push scanf_format
    call scanf
    add esp,8
    finit
_sinh: ; ( 2^(x*log2e) - 2^(-x*log2e) ) / 2
    fldl2e
    fld QWORD [argument] ;x*log2e
    fmulp
    call powerof2 ;2^(x*log2e)
    fxch ; replace st0 with st1 and st1 with st0
    fchs ; -(x*log2e)xch
    call powerof2 ;2^(-x*log2e)
    
    fstp
    fsubp
    fld1
    fld1
    faddp ;2
    fdivp  ; thing / 2
    fstp QWORD [sinh]
_arcsinh:
    fld QWORD [argument]
    fld st0
    fmulp 
    fld1
    faddp
    fsqrt
    fld QWORD [argument]
    faddp
    call naturallog
    fstp QWORD[arcsinh]



    jmp print
powerof2:
    fstcw WORD [SaveCW] ;Zmien rejestr kontrolny i wlacz zaokraglanie przez obciecie
    fstcw WORD [MaskedCW]
    or byte [MaskedCW+1], 1100b
    fldcw WORD [MaskedCW]
    fld st0 ;duplikacja st0
    fld st0
    frndint ;oblicz czesc calkowita
    fxch ;zamien
    fsub st0, st1 ;oblicz czesc ukamkowa
    f2xm1 ;oblicz 2**fracx-1
    fld1
    faddp ;oblicz 2'*fracx
    fxch ;we2 czeSC calkowita
    fld1 ;oblicz 1*2*intx
    fscale
    fstp st1 ;pop st1 t jest 1)
    fmulp ;oblicz 2'*intx ' 2**fracx
    fldcw WORD [SaveCW] ;Restore rounding mode
    ret
naturallog:
    fld1 ; we have st0 and we load 1
    fxch 
    fyl2x ; 1 * log2x
    fldl2e
    fdivp ; log2x/log2e = logex
    ret
print:
    push DWORD [arcsinh+4]
    push DWORD [arcsinh]
    push DWORD [sinh+4]
    push DWORD [sinh]
    push output_format
    call printf
    add  esp,20