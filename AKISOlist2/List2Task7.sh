#!/bin/bash
for line in *;
do
  if [ -f "$line" ] && [ "$line" != "${line,,}" ]; then
    mv -i -- "$line" "${line,,}";
  fi
done;
#using pipe: 
#ls | grep [[:upper:]] | xargs -n1 -I filename bash -c 'mv filename $(echo filename | tr "[:upper:]ĄĘÓĆŃŻŹŚŁ" "[:lower:]ąęóćńżźśł")';
