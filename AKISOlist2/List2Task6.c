#include <stdio.h>

int main(int argc, char **args)
{
    for(int i = 0; i < 256; i++)
    {
        const char* text = "Hello, World!\n";
        printf("\e[38;5;%dm%s", i, text);
    }
    printf("\n");
}
