#!/bin/bash
trap ctrl_c INT;
ctrl_c() { tput cup $(($max_graph_height+1)) 0; tput cnorm; exit; } 
print_bytes()
{
    if [ $1 -lt 1001 ]; then
        echo   "${1}bytes/s";
    elif [ $1 -lt 1000001 ]; then
        printf "%.2f" "$( echo '$1 / 1000' | bc -l )Kb/s";
    else
        printf "%.2f" "$( echo '$1 / 1000000' | bc -l )Mb/s";
    fi;
}

#IFS=$'\n'
dev_info="$(cat /proc/net/dev )";
#echo "$dev_info"
time_gap=1;
upper_margin=6;
row_counter=$upper_margin;
real_graph_height=10;
max_graph_height=$(($upper_margin+$real_graph_height));
graph_width=10;
tmp_width=0;
array=();
count=0;
avg_bytes_two=0;

#initialize array with zeros
for (( i=0; i < graph_width; i++))
do
    array[$i]=0;
done;


update_array() {
    array=("${array[@]:1}") # stworz tablice array z tego co w nawiasach, to w nawiasach: wez wszstkie argumenty array i pomin 1 pierwszy
    # for (( i=0; i < $graph_width-1; i++))
    # do
    #     array[$i]=${array[$(($i+1))]:-0};
    #     #echo "++${array[$i]}++"
    # done;
    ((array[$graph_width-1]=${1}));
    
    #((array[$graph_width-1]=$avg_bytes_two));
}
get_max_from_array() {
    local max=0
    for val in "${array[@]}";
    do
        (( val > max )) && max=tmp;
    done;
    # for (( i=0; i < ${#array[@]}; i++))
    # do
    #     tmp=${array[$i]};
    #     if [ tmp -gt max ]; then max=tmp; fi;
    # done;
    echo $max;
}
print_everything() {
    #RUNTIME
    runtime="$( cat /proc/uptime | awk -F' ' '{ print $1 }')";
    printf "Runtime:     %d days %d hours %d minutes %2.0f seconds.                  \n" \
        "$( echo "scale=0;$runtime/60.0/60.0/24 " | bc -l )" \
        "$( echo "scale=0;($runtime/60.0/60.0)%24 " | bc -l )" \
        "$( echo "scale=0;($runtime/60.0)%60 " | bc -l )" \
        "$( echo "scale=0;$runtime%60" | bc -l )";
    #SPEED
    bytes=$( cat /proc/net/dev | tail -n +3 | awk '
    { bytes += $2 + $10 }
    END { print bytes } ' );
    bytes_old=${bytes_old:-$bytes} #jezeli bytes_old sa puste to przypisuje bytes jezeli nie to nic nie robi
    avg_bytes=$(( ($bytes - $bytes_old) / $time_gap ));
    printf "Total speed: %s               \n" "$( print_bytes $avg_bytes )";
    
    #AVG SPEED
    #echo ${#array[@]}; #length of array
    (( avg_bytes_two=($avg_bytes_two*$count+$avg_bytes)/($count+1) ))
    (($count+1));
    printf "Average speed: %s  \n" "$( print_bytes $avg_bytes_two)";

    #BATTERY
    echo   "Battery:     $( cat /sys/class/power_supply/battery/capacity )%                          ";

    #SYSTEM LOAD
    printf "System load: (over last one minute) %s%%, (over last five minutes) %s%%.           \n" \
        $( cat /proc/loadavg | awk -F' ' '{print $1*100} {print $2*100}' );

    #GRAPH
    tput cup $upper_margin 0; 
    printf "$( print_bytes $max_bytes )";
    tput cup $max_graph_height 0; printf "0bytes/s";
    #$tmp_width=0;
    for (( i=0; i < $graph_width; i++))
    do
        limit=$(( $max_graph_height-${array[$i]}/$scale ));
        #row_counter=$max_graph_height;
        column=$((10+$i*3));
        for (( row_counter=$max_graph_height; row_counter >= $upper_margin; row_counter-- ))
        do
            tput cup $row_counter $column;
            if [ $row_counter -gt $limit ]; then
                tput setab 5; 
            fi;
                printf "   ";
                tput sgr0;
        done;
    done;
}
# tput civis;
tput init;
#tput civis;
    avg_bytes=0;
while true;
do
    max_bytes=$(get_max_from_array);
    scale=$( echo "scale=0;$max_bytes/$real_graph_height" | bc -l );
    (( scale = scale > 0 ? scale : 1 ))
    tput rc;
    tput civis;
    sleep $time_gap;
    #update_array $(($avg_bytes*$scale));
    #echo "scale=0;($RANDOM%100)/$scale" | bc -l;
    update_array "$( echo "scale=0;$avg_bytes" | bc -l )";
    printf '%s'  "$( print_everything; )"
    tput cnorm;
done;