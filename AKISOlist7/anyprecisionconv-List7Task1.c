#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    if (argc < 3)
        exit(1);
    int precision = atoi(argv[2]);
    int whole = atoi(argv[1]);
    unsigned long long number = atoll(index(argv[1], '.') + 1);
    int len = strlen(index(argv[1], '.') + 1);
    unsigned long long limit = 1;

    //integer
    bool any = false;
    for(int i = sizeof(whole) * 8 - 1; i >= 0; i--)
    {
        if (whole & (1 << i))
        {
            any = true;
            printf("1");
        }
        else if(any)
            printf("0");
    }
    printf(".");
    //fraction
    for (int i = 0; i < len; i++)
        limit *= 10;    
    for (int i = 0; i < precision; i++)
    {
        number *= 2;
        if (number >= limit)
        {
            printf("1");
            number -= limit;
        }
        else
        {
            printf("0");
        }
        if (number == 0)
            break;
    }
    printf("\n");
}